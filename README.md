# CI/CD 
[![pipeline status](https://gitlab.com/thieentv/cicd-started/badges/master/pipeline.svg)](https://gitlab.com/thieentv/cicd-started/-/commits/master)

# Build docker image
- docker build -t cicd-started:v1 .
- docker-compose up -d

# Push image to docker registry
- docker tag cicd-started:v1 registry.gitlab.com/thieentv/cicd-started
- docker login registry.gitlab.com
- docker push registry.gitlab.com/thieentv/cicd-started

